Source: python-odoorpc
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Philipp Huebner <debalance@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               python3-sphinx,
               python3-all,
               python3-setuptools,
               dh-python
Homepage: https://pythonhosted.org/OdooRPC/
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/python-odoorpc.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-odoorpc
Testsuite: autopkgtest-pkg-python

Package: python3-odoorpc
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Provides: ${python3:Provides}
Description: pilot Odoo servers through RPC (Python 3)
 OdooRPC is a Python module providing an easy way to pilot your Odoo
 (formerly known as OpenERP or TinyERP) servers through RPC.
 .
 Features supported:
  - access to all data model methods (even browse) with an API similar
    to the server-side API,
  - use named parameters with model methods,
  - user context automatically sent providing support for
    internationalization,
  - browse records,
  - execute workflows,
  - manage databases,
  - reports downloading,
  - JSON-RPC protocol (SSL supported)
 .
 OdooRPC is a modern alternative to the older OERPLib by the same
 author. It supports both Python 2 and 3.
 .
 This package provides the module for Python 3.x.

Package: python-odoorpc-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: pilot Odoo servers through RPC (documentation)
 OdooRPC is a Python module providing an easy way to pilot your Odoo
 (formerly known as OpenERP or TinyERP) servers through RPC.
 .
 Features supported:
  - access to all data model methods (even browse) with an API similar
    to the server-side API,
  - use named parameters with model methods,
  - user context automatically sent providing support for
    internationalization,
  - browse records,
  - execute workflows,
  - manage databases,
  - reports downloading,
  - JSON-RPC protocol (SSL supported)
 .
 OdooRPC is a modern alternative to the older OERPLib by the same
 author. It supports both Python 2 and 3.
 .
 This package provides the documentation and an example.
